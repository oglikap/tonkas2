---
layout: productie
naam: jabroer
title: Jabroer
landing-title: '2015/2016'
description: 'Tournee 2015/2016'
image: /assets/images/sandy.jpg
author: null
categories: [tonk]
article-header: Interview B.N. n.a.v. Jabroer
---
**_Mogen we het cabaret noemen?_**<br />
Je mag het noemen zoals je wilt, maar cabaret lijkt me het meest voor de hand liggen. Volgens Wikipedia zijn er grofweg 10 soorten cabaret te onderscheiden, van verhalend tot fysiek.
Daar blijkt dus al dat ze er in de discipline zelf ook nog niet helemaal over uit zijn. Ik gok dat ik val onder de noemer 'Nonsens cabaret', maar het zal me eerlijk gezegd worst wezen.

**_Maar je hebt dus wel iets met moppen?_**<br />
Ik heb iets met wat ik ermee doe: ik reduceer ze tot de kern, herschrijf ze, voeg er wat lagen aan toe, betrek ze op mezelf en maak er een chronologisch verhaal van waarin ze in hoog tempo voorbij komen.
De werking ontstaat verder grotendeels door de veelheid, niet door de afzonderlijke mopjes.

**_Beter goed gejat, dan slecht bedacht?_**<br />
Hergebruik is het uitgangspunt ja, maar het is niet zo dat er niets bedacht hoeft te worden, zo makkelijk kom ik er helaas niet mee weg...
Maar na 25 jaar oorspronkelijk werk te hebben geschreven leek het me een provocerende grap om er als motto 'puur jatwerk' onder te schrijven

**_Je bent nu ‘die moppentapper’._**<br />
Ik heb al zoveel gezichten, dit kan er ook nog wel bij.

**_Wat is het geheim van een goeie mop?_**<br />
In mijn geval: dat ik 'm vertel zonder dat ik er zelf lol in heb. Hoe chagrijniger hoe beter.

**_Liefst vrouwonvriendelijk en incorrect?_**<br />
Ik ken geen 'vrouwvriendelijke' moppen, een grap moet in zekere zin altijd incorrect zijn.

**_Blijft apart; een moppentapper die niet van moppen houdt._**<br />
Nou ja, ik was al een toneelspeler die niet van toneel houdt, dus dit was geen grote stap.
Mijn lol zit 'm voornamelijk in het effect dat het bij de mensen sorteert.

**_Je staat te boek als notoir 'zwartkijker'. Humor is het medicijn om je door het leven te slaan? Humor als troost?_**<br />
Als ik me niet vergis zijn humoristen doorgaans niet de meest opgewekte, levenslustige creaturen. In zoverre pas ik aardig in het rijtje.
Op de radio hoorde ik laatst iemand zeggen dat cynisme een vorm is van moreel lui denken. Die mevrouw was op het onderwerp afgestudeerd, dus dan zal ze wel gelijk hebben...
Toch is mijn insteek dat je vermoedelijk een paar dingen over het hoofd ziet, als je het leven als één grote fuif ervaart.
Nog afgezien daarvan ken ik eigenlijk geen blijmoedige, opgeruimde types waar ik ook nog om kan lachen.
Dus zelfs als we mijn zwartgalligheid vanuit wetenschappelijk oogpunt als een handicap moeten bestempelen, dan ben ik er eerlijk gezegd nog steeds best tevreden mee. Zonder dat zou ik mijn werk niet kunnen doen.
