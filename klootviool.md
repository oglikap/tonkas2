---
layout: productie
naam: klootviool
title: Klootviool
landing-title: '2013/2014'
description: 'Tournee 2013/2014'
image: assets/images/sandy.jpg
author: null
categories: [tonk]
article-header:
---
<section style="height: 4em;"></section>
4 maart 2013.

UITNODIGING

Beste vrienden, kennissen, collega's, enz.

Ik doe 3 previews KLOOTVIOOL in De Kleine Komedie, dat is 14, 15 en 16 maart.

Het hele eiereten is bedoeld om 's te kijken of dit per ongeluk iets is om mee door te gaan.<br />
Er is verder geen tournee gepland, ook wel 'ns lekker.

Ik repeteer niks, dus ik heb geen idee wat dit wordt, hopelijk een melige bende.<br />
Op papier is 't een regelrechte giller, om zo maar te zeggen.

Leuk als je komt.

Je kunt tot 7 maart kaarten bestellen via bunkertheaterzaken.nl<br />
Daarna via dekleinekomedie.nl

Groet, Ton Kas

_Ik bedank:  Hans van der Zijpp, Esther Weimann, Chantal Rens, Helga Voets, Kees van de Lagemaat, Tone of Voice, Diederik Ebbinge, Bert Hogenes, Hans Huiskes._
